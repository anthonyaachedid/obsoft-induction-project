const clss = document.getElementById('tableClasses');

if (clss) {
  clss.addEventListener('click', (e) => {
    if (e.target.className === 'btn btn-danger delete-class') {
      if (confirm('Are you sure?')) {
        const id = e.target.getAttribute('data-id');

        fetch(`/classes/delete/${id}`, {
          method: 'DELETE'
        }).then(res => window.location.reload());
      }
    }
  });
}