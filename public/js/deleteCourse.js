const courses = document.getElementById('tableCourses');

if (courses) {
  courses.addEventListener('click', (e) => {
    if (e.target.className === 'btn btn-danger delete-course') {
      if (confirm('Are you sure?')) {
        const id = e.target.getAttribute('data-id');

        fetch(`/courses/delete/${id}`, {
          method: 'DELETE'
        }).then(res => window.location.reload());
      }
    }
  });
}