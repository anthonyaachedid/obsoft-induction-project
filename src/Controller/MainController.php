<?php
    namespace App\Controller;

    use App\Entity\Student;
    use App\Entity\ClassRoom;
    use App\Entity\Course;
    use App\Entity\Grades;

    use App\Form\GradesCourseType;
    use App\Form\GradesStudentType;
    use App\Form\CourseType;

    use App\Repository\GradesRepository;

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\Routing\Annotation\Route;
    use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
    use Symfony\Component\HttpFoundation\Request;

    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;
    use Vich\UploaderBundle\Form\Type\VichImageType;
    use Symfony\Component\Form\Extension\Core\Type\DateType;
    use Symfony\Component\Form\Extension\Core\Type\TextareaType;
    
    class MainController extends AbstractController {
        
        /**
         * @Route("/",methods={"GET"})
         */
        public function index(){
            return $this->render('twigs/home.html.twig');
        }

       
       
        /**
         * @Route("/student/new",name="NEW",methods={"GET","POST"})
         */
        public function newStudent(Request $request, GradesRepository $gradesRepository){
            
            
            $cls =new ClassRoom();
            $cls->setId(3);
            $cls->setName('class3');
            $cls->setSection("section1");
            
            dump($cls);
            dump($gradesRepository->findOneBySomeField($cls));
                
            
            return $this->render('twigs/student/newStudent.html.twig');
        }

        /**
         * @Route("/students/delete/{id}",methods={"DELETE"})
         */
        public function delete(Request $request, $id) {
            $student = $this->getDoctrine()->getRepository
            (Student::class)->find($id);
      
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($student);
            $entityManager->flush();
      
            $response = new Response();
            $response->send();
          }

        /**
         * @Route("/students/edit/{$id}",methods={"GET","POST"})
         */
        public function editStudent(Request $request, $id){
            $student=new Student();
            $student=$this->getDoctrine()->getrepository(Student::class)->find($id);

            $form=$this->createFormBuilder($student)
             ->add('firstName',TextType::class,array('attr'=>array('class'=>'form-control')))
             ->add('lastName',TextType::class,array('attr'=>array('class'=>'form-control')))
             ->add('dateOfBirth',DateType::class, ['widget' => 'single_text','format' => 'yyyy-MM-dd',])
             ->add("imageFile",VichImageType::class, array('label' => 'Update Image'))
             ->add('save',SubmitType::class, array('label'=>'Update','attr'=>array('class'=>'btn btn-primary nt-3')))
             ->getForm();
            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){
                
                $entityManager=$this->getDoctrine()->getManager();
                $entityManager->flush();
                
                return $this->redirectToRoute('students');

            }
            return $this->render('twigs/student/editStudent.html.twig',array('form'=> $form->createView()));
        }
         
        /**
         * @Route("/students",name="students",methods={"GET"})
         */
        public function Students(){
        
            $students=$this->getDoctrine()->getRepository(Student::class)->findAll();
            return $this->render('twigs/student/students.html.twig',array('students'=>$students));

        }


        

        /**
         * @Route("/students/{id}", name="student_show")
         */
        public function show($id) {
            $student = $this->getDoctrine()->getRepository(Student::class)->find($id);
      
            return $this->render('twigs/student/infoStudent.html.twig', array('student' => $student));
          }


    ##################################################################################################################
    ##################################################################################################################
    ##################################################################################################################
        
        /**
         * @Route("/class/new",name="newClass",methods={"GET","POST"})
        */
        public function newClass(Request $request){
            $clss=new ClassRoom();

            $form=$this->createFormBuilder($clss)
             ->add('name',TextType::class,array('attr'=>array('class'=>'form-control')))
             ->add('section', TextType::class, array('attr'=>array('class'=>'form-control')))
             ->add('save',SubmitType::class, array('label'=>'Create','attr'=>array('class'=>'btn btn-primary nt-3')))
             ->getForm();
            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){
                $clss=$form->getData();
                
                $entityManager=$this->getDoctrine()->getManager();
                $entityManager->persist($clss);
                $entityManager->flush();
                
                return $this->redirectToRoute("classes");

            }
            return $this->render('twigs/class/newClass.html.twig',array('form'=> $form->createView()));
        }


        /**
         * @Route("/classes/delete/{id}",methods={"DELETE"})
         */
        public function deleteClassRoom(Request $request, $id) {
            $classroom = $this->getDoctrine()->getRepository
            (ClassRoom::class)->find($id);
      
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($classroom);
            $entityManager->flush();
      
            $response = new Response();
            $response->send();
          }
    
    
    
        /**
         * @Route("/classes",name="classes",methods={"GET"})
         */
        public function Classes(){
        
            $clss=$this->getDoctrine()->getRepository(ClassRoom::class)->findAll();

            return $this->render('twigs/class/classes.html.twig',array('classes'=> $clss));

        }

    ##################################################################################################################
    ##################################################################################################################
    ##################################################################################################################
        
        /**
         * @Route("/course/new",name="newCourse",methods={"GET","POST"})
         * @param Request $request
         * @return \Symfony\Component\HttpFoundation\Response
        */
        public function newCourse(Request $request){
            $grade =new Grades();
            $course=new Course();
            

            $course->setName("course");
            $course->setDescription("course");
            $grade->setCourse($course);
            

            $form =$this->createForm(GradesCourseType::class, $grade);
            $form->handleRequest($request);

            if($form-> isSubmitted() && $form->isValid()) {
                $entityManager=$this->getDoctrine()->getManager();
                $entityManager->persist($course);
                $entityManager->persist($grade);
                $entityManager->flush();
                
                return $this->redirectToRoute("courses");
            }
            return $this->render('twigs/course/newCourse.html.twig',array('form'=> $form->createView()));
        }


        /**
         * @Route("/courses/delete/{id}",methods={"DELETE"})
         */
        public function deleteCourse(Request $request, $id) {
            $course = $this->getDoctrine()->getRepository
            (Course::class)->find($id);
      
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($course);
            $entityManager->flush();
      
            $response = new Response();
            $response->send();
          }



        /**
         * @Route("/courses",name="courses",methods={"GET"})
         */
        public function Course(){
        
            $courses=$this->getDoctrine()->getRepository(Course::class)->findAll();

            return $this->render('twigs/course/course.html.twig',array('courses'=> $courses));

        }
}