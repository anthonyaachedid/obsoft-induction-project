<?php

namespace App\Entity;

use App\Entity\Student;
use App\Entity\Course;
use App\Entity\ClassRoom;



use App\Repository\GradesRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Form\FormTypeInterface;

/**
 * @ORM\Entity(repositoryClass=GradesRepository::class)
 */
class Grades
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\OneToOne(targetEntity=ClassRoom::class, inversedBy="grades", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="classroom_id",referencedColumnName="id")
     */
    private $classroom;

 

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStudent(): ?student
    {
        return $this->student;
    }

    public function setStudent(?student $student): self
    {
        $this->student = $student;

        return $this;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(?Course $course): self
    {
        $this->course = $course;

        return $this;
    }

    public function getClassroom(): ?ClassRoom
    {
        return $this->classroom;
    }

    public function setClassroom(?ClassRoom $classroom): self
    {
        $this->classroom = $classroom;

        return $this;
    }
}
