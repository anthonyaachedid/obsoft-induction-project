<?php

namespace App\Form;

use App\Entity\Grades;
use App\Entity\ClassRoom;
use App\Entity\Course;
use App\Entity\Student;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class GradesCourseType extends AbstractType
{   
    /**
    *{@inheritdoc}
    */
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('course', CourseType::class)
        ->add('classroom',EntityType::class, [
            'class'=> ClassRoom::class ,
            'choice_label' => 'name'])

        ->add('save', SubmitType::class, [
            'attr' => ['class' => 'btn btn-success']
        ]);
    }

    /**
    *{@inheritdoc}
    */

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Grades::class,
        ]);
    }
}
